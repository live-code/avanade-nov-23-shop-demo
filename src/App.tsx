import { useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import { NavBar } from './core/NavBar.tsx';
import { useCart } from './core/store/use-cart.ts';

function App() {

  const initCart = useCart(state => state.init)

  useEffect(() => {
    initCart();
  }, []);

  return (
    <>
      <NavBar />

      <div className="max-w-3xl mx-auto p-3">
       <Outlet />
      </div>
    </>
  )
}

export default App
