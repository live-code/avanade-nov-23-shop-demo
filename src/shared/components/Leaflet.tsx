import { useEffect, useRef } from 'react';

import L from 'leaflet';

interface LeafletProps {
  coords: [number, number];
  zoom?: number
}
export function Leaflet(props: LeafletProps) {
  const mapRef = useRef<HTMLDivElement | null>(null);
  const map = useRef<L.Map | null>(null)

  const { coords, zoom = 10 } = props;

  useEffect(() => {
    if (map.current && mapRef.current) {
      map.current = L.map(mapRef.current)
        .setView(coords, zoom);

      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(map.current);
    }

  }, []);

  useEffect(() => {
    map.current?.setView(coords)
  }, [coords]);

  useEffect(() => {
    map.current?.setZoom(zoom)
  }, [zoom]);

  return <div>
    <div ref={mapRef} className="map"></div>
  </div>
}
