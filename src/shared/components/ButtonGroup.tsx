import clsx from 'clsx';
import { PropsWithChildren } from 'react';

interface ButtonGroupProps {
  direction?: 'row' | 'column'
}

export function ButtonGroup(props: PropsWithChildren<ButtonGroupProps>) {
  const {
    direction = 'row',
    children
  } = props;
  return <div className={clsx(
    'flex  gap-4',
    {
      'flex-row': direction === 'row',
      'flex-col': direction === 'column',
    }
  )}>

    {children}
  </div>
}
