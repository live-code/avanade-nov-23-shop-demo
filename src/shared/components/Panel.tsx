import clsx from 'clsx';
import React, { CSSProperties, PropsWithChildren } from 'react';

type PanelProps = {
  title: React.ReactNode;
  titleBarCls?: string;
  titleBarStyles?: CSSProperties;
}

export function Panel(
  props: PropsWithChildren<PanelProps>
) {

  const {
    title,
    titleBarCls = 'bg-black text-white',
    titleBarStyles,
    children
  } = props;

  return <div className="w-full border border-black">
    <div
      className={clsx(
        'p-2',
        titleBarCls,
      )}
      style={ titleBarStyles  }
    >
      {title}
    </div>
    <div className="p-2">
      {children}
    </div>
  </div>
}
