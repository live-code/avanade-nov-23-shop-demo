import clsx from 'clsx';
import { PropsWithChildren } from 'react';

interface TitleProps {
  size?: 'sm' | 'lg' | 'xl'
}

export function Title(props: PropsWithChildren<TitleProps>) {

  const {
    size = 'xl'
  } = props;

  return (
    <h1
      className={clsx(
        'my-4',
        {
          'text-4xl border-b border-black border-dashed': size === 'xl',
          'text-2xl': size === 'lg',
          'text-base font-bold': size === 'sm',
        }
      )}
    >
      {props.children}
    </h1>
  )
}
