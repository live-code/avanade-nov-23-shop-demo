import clsx from 'clsx';
import { PropsWithChildren } from 'react';

interface FxProps {
  gap?: 0 | 1 | 4 | 8;
  justify?: 'justify-start' | 'justify-end' | 'justify-center' | 'justify-between' | 'justify-around' | 'justify-evenly';
  valign?: 'items-start' |'items-end' |'items-center' |'items-baseline' |'items-stretch';
}
export function Fx(props: PropsWithChildren<FxProps>) {

  const {
    gap = 0,
    justify = 'justify-center',
    valign = 'items-start'
  } = props

  return (
    <div
      className={clsx(
        'flex',
        justify,
        valign
      )}
      style={{ gap: gap }}
    >
      {props.children}
    </div>
  )

}
