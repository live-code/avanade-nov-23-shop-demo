import React from 'react';

const baseURL = 'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn';

interface MapQuestProps {
  city: string;
  zoom: number;
}

export function MapQuest(props: MapQuestProps & React.ImgHTMLAttributes<HTMLImageElement>) {
  const {
    city,
    zoom = 10,
    width = '100%',
    ...rest
  } = props;

  const url = `${baseURL}&center=${city}&size=600,400&zoom=${zoom}`

  return <>
    <img src={url} width={width} {...rest}/>
  </>
}
