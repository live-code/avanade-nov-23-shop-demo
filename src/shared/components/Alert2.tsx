import clsx from 'clsx';
import { PropsWithChildren } from 'react';

interface AlertProps {
  variant?: 'primary' | 'warn' | 'success';
  onClose?: () => void;
}


export function Alert2(props: PropsWithChildren<AlertProps>) {

  const {
    variant = 'primary',
    children,
    onClose
  } = props;

  return <div
    className={clsx(
      'p-2 my-6 border-2 border-black rounded-xl',
      {
        'bg-red-600': variant === 'warn',
        'bg-green-600': variant === 'success',
        'bg-blue-600': variant === 'primary',
      }
    )}
  >
    <div className="flex justify-between">
      {children}

      {
        onClose &&
          <i
            className="fa fa-times"
            onClick={onClose}
          ></i>
      }
    </div>
  </div>
}

// export const Alert = Alert2;
