import clsx from 'clsx';
import React from 'react';

interface ButtonCustomProps {
  icon?: string;
}

type ButtonProps = ButtonCustomProps & React.ButtonHTMLAttributes<HTMLButtonElement>

export function Button(props: ButtonProps) {

  const {
    icon,
    className,
    children,
    ...rest
  } = props;

  return <button
    {...rest}
    className={clsx('btn', className)}
  >
    <i className={icon}></i>
    {children}
  </button>
}
