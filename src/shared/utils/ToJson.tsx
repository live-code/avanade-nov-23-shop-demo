export interface ToJsonProps {
  value: any;
}

export function ToJson(props: ToJsonProps) {
  return <pre className="bg-gray-300 rounded-xl">
    {JSON.stringify(props.value, null, 2)}
  </pre>
}
