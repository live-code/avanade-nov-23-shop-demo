export type Product = {
  id: number;
  name: string;
  cost: number;
  img: string;
  type: ProductType
}

export type ProductType = 'fresco' | 'confezionato'
