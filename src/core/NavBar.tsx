import { NavLink, useNavigate } from 'react-router-dom';
import { useCartSummaryPanel } from './store/use-cart-summary-panel.ts';
import { useCart } from './store/use-cart.ts';

export function NavBar() {
  const isPanelOpened = useCartSummaryPanel(state => state.isOpen)
  const togglePanel = useCartSummaryPanel(state => state.toggle)
  const closePanel = useCartSummaryPanel(state => state.close)
  const cartTotal = useCart(state => state.list.length)
  const cartItems = useCart(state => state.list)
  const cartTotalCost = useCart(
    state => state.list.reduce((acc, item) => acc + (item.product.cost * item.qty), 0)
  )
  const navigate = useNavigate()

  function gotoCart() {
    closePanel()
    navigate('/cart')
  }
  return (
    <div className="sticky top-0 z-10 bg-white bg-opacity-80 shadow-2xl">
      {/*NAVBAR*/}

      <div className="flex items-center justify-between p-3 ">

        {/* NAV BUTTONS*/}
        <div className="flex gap-3">
          <NavLink to='home' className="btn">Home</NavLink>
          <NavLink to='shop' className="btn">Shop</NavLink>
          <NavLink to='uikit' className="btn">Ui Kit</NavLink>
        </div>

        {/*BADGE CART */}
        <div
          className="bg-black text-white rounded-xl px-2 py-1"
          onClick={togglePanel}
        >
          {cartTotal} prodotti (€ {cartTotalCost})
        </div>
      </div>

      {/*CART SUMMARY*/}
      {
        isPanelOpened &&
          <div className="bg-white shadow-lg w-96 fixed right-0 p-4">
            {
              cartTotal === 0 && <div>No product in cart</div>
            }
            {
              cartItems.map(item => {
                const { qty, product: { id, name, cost } } = item;
                return <li className="flex justify-between" key={id}>
                  <div>{name} </div>
                  <div>€ {cost} * {qty}pz | € {cost * qty}</div>
                </li>
              })
            }
            {
              cartTotal > 0 &&
                <button  onClick={gotoCart} className="btn">Go to Cart</button>
            }
          </div>
      }
    </div>
  )
}
