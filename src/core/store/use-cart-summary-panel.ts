import { create } from 'zustand';

export interface UseCartSummaryPanelState {
  isOpen: boolean;
  open: () => void;
  close: () => void;
  toggle: () => void;
}

export const useCartSummaryPanel = create<UseCartSummaryPanelState>((set, get) => ({
  isOpen: false,
  open: () => {
    set(() => ({ isOpen: true}))
  },
  close: () => {
    set(() => ({ isOpen: false}))
  },
  toggle: () => {
    set(() => ({ isOpen: !get().isOpen}))
  }
}))
