import { create } from 'zustand';
import { Product } from '../../model/product.ts';

export type CartItem = {
  product: Product;
  qty: number
}

export interface CartState {
  list: CartItem[];
  init: () => void;
  addToCart: (item: Product) => void;
  removeFromCart: (id: number) => void;
  increment: (productId: number) => void;
  decrement: (productId: number) => void;
  clear: () => void;
}


export const useCart = create<CartState>((set, get) => ({
  list: [],
  init: () => {
    const itemsFromLocalstorate = localStorage.getItem('items');
    if (itemsFromLocalstorate) {
      set({ list: JSON.parse(itemsFromLocalstorate)})
    }

  },
  addToCart: (productToAdd: Product) => {
    const productInCart = get().list.find(item => item.product.id === productToAdd.id)
    if (productInCart) {
      get().increment(productToAdd.id)
    } else {
      // if not exists
      const cartItem: CartItem = {
        product: productToAdd,
        qty: 1
      }
      set((state) => ({ list: [...state.list, cartItem] }))

    }
    saveLocalStorage(get().list)
  },
  removeFromCart: (id: number) => {
    console.log(id)
    set(state => ({
      list: state.list.filter(p => p.product.id !== id)
    }))
    saveLocalStorage(get().list)

  },
  increment: (productId: number) => {
    const productInCart = get().list.find(item => item.product.id === productId)
    if (productInCart) {
      // inc qty
      productInCart.qty += 1;
      set((state) => ({
        list: state.list.map(item => {
          if (item.product.id === productId) {
            return productInCart;
          }
          return item
        })
      }))
    }
    saveLocalStorage(get().list)
  },
  decrement: (productId: number) => {
    const productInCart = get().list.find(item => item.product.id === productId)

    if (productInCart && productInCart.qty === 1) {
      get().removeFromCart(productId)
    }
    if (productInCart && productInCart.qty > 1) {
      // inc qty
      productInCart.qty -= 1;
      set((state) => ({
        list: state.list.map(item => {
          if (item.product.id === productId) {
            return productInCart;
          }
          return item
        })
      }))
    }
    saveLocalStorage(get().list)
  },
  clear: () => {
    set(() => ({ list: []}))
    saveLocalStorage(get().list)
  }
}))

function saveLocalStorage(data: any) {
  localStorage.setItem('items', JSON.stringify(data))
}

