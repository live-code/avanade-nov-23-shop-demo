import { useCart } from '../../core/store/use-cart.ts';
import { Panel } from '../../shared/components/Panel.tsx';
import { Title } from '../../shared/components/Title.tsx';

export function Cart() {
  const cartItems = useCart(state => state.list)
  const removeFromCart = useCart(state => state.removeFromCart)
  const increment = useCart(state => state.increment)
  const decrement = useCart(state => state.decrement)

  return (
    <div>
      <Title>Cart Summary</Title>

      <div>
        {
          cartItems.map(item => {
            const { product, qty} = item;
            return <li key={product.id} className="flex justify-between gap-3">
              <div>
                {product.name} -  € {product.cost}
              </div>
              <div>
                <button
                  className="btn"
                  onClick={() => decrement(item.product.id)}
                >-</button>
                qty: {qty}
                <button
                  className="btn"
                  onClick={() => increment(item.product.id)}
                >+</button>

              </div>
              <div>

                € {product.cost * qty}
                <i
                  onClick={() => removeFromCart(item.product.id)}
                  className="fa fa-trash"></i>
              </div>

            </li>
          })
        }
      </div>


      <br/>
      <br/>
      <br/>
      <br/>

      <div className="grid grid-cols-2 gap-3">
        {
          cartItems.map(item => {
            const { product, qty} = item;
            return (
              <Panel key={product.id} titleBarCls="bg-pink-500 text-white" title={
                <div className="flex justify-between">
                  <div>{product.name}</div>

                  <div>
                    <button
                      className="btn"
                      onClick={() => decrement(item.product.id)}
                    >-</button>
                    qty: {qty}
                    <button
                      className="btn"
                      onClick={() => increment(item.product.id)}
                    >+</button>

                  </div>

                  <i
                    onClick={() => removeFromCart(item.product.id)}
                    className="fa fa-trash"></i>
                </div>
              }>

                ...
                  € {product.cost * qty}


              </Panel>
            )
          })
        }
      </div>
    </div>
  )
}
