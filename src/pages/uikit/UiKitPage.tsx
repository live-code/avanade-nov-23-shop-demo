import { useEffect, useState } from 'react';
// From Barrel
import { Title, Alert, Button, Fx, Panel } from '../../shared';

import * as UI from '../../shared';


export function UiKitPage() {
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    // .... server

    setTimeout(() => {
      setVisible(true)
    }, 2000)
  }, []);

  return <div>
    <UI.Title>UIKIT demo</UI.Title>

    <Title size="lg">Button</Title>
    <Button />

    <Title size="lg">Fx (Flex)</Title>

    <Fx justify="justify-between" valign="items-center">
      <div className="bg-red-200">
        lorem <br/>ipsum
      </div>
      <div  className="bg-cyan-200">2</div>
      <div className="bg-green-400">3</div>
    </Fx>

    <Fx gap={8} justify="justify-between">
      <div>1</div>
      <div>2</div>
      <div>3</div>
    </Fx>


    <div className="flex justify-end gap-3">
      <div>one</div>
      <div>two</div>
      <div>three</div>
    </div>


    <Title size="lg">Alert component</Title>

    <Fx gap={8} justify="justify-between">
      { visible && <Alert onClose={() => setVisible(false)}>lorem 1 ...</Alert>}
       <Alert variant="primary" >lorem 2...</Alert>
       <Alert variant="warn" >lorem 3...</Alert>
       <Alert variant="success" >lorem 4...</Alert>
    </Fx>

    <Title size="lg">Panel Component</Title>


    <Panel
      title="example 1"
      titleBarCls="bg-green-100 text-red-400"
      titleBarStyles={{ background: 'red', color: 'white', border: '4px solid black'}}
    >
      bla bla
    </Panel>

    <Panel title="example 2">
      bla bla
    </Panel>



    <br/>
    <br/>
    <br/>
    <br/>







    <Panel
      title="hello world"
    >
      <input placeholder="bla bla" />
      <input placeholder="bla bla" />
      <input placeholder="bla bla" />

      <div className="flex justify-between gap-3">
        <div className="w-1/2">
          <Panel title="left">
            bla bla
          </Panel>
        </div>

        <div className="w-1/2">
          <Panel title="right">
            bla bla
          </Panel>
        </div>
      </div>
    </Panel>


    <Title size="lg">Title Component</Title>

    <Title size="xl">XL</Title>
    <Title size="lg">LG</Title>
    <Title size="sm">SM</Title>

  </div>
}
