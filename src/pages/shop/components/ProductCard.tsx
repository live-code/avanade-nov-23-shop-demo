import clsx from 'clsx';
import { Product } from '../../../model/product.ts';

type ProductCardProps = {
  product: Product;
  onAddToCart: () => void;
}

export function ProductCard(props: ProductCardProps) {
  const { product: p } = props;

  return (
    <div  className="w-full relative">
      <img src={p.img} alt="" className="w-full"/>

      <div className="absolute bottom-0 bg-white bg-opacity-70 w-full p-3">
        <div className="flex justify-between items-center">
          <div className="flex items-center gap-3">
            {
              <i className={clsx('fa', {
                'fa-archive fa-2x text-red-500': p.type === 'confezionato',
                'fa-lemon-o fa-2x text-green-500': p.type === 'fresco',
              })}></i>
            }
            {p.name}
          </div>

          <div className="flex gap-2 items-center">
            € {p.cost}
            <i
              onClick={props.onAddToCart}
              className="fa fa-cart-plus fa-2x"></i>

          </div>


        </div>
      </div>

    </div>
  )
}
