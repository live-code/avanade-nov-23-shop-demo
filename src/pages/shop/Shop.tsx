import { useLoaderData } from 'react-router-dom';
import { useCartSummaryPanel } from '../../core/store/use-cart-summary-panel.ts';
import { useCart } from '../../core/store/use-cart.ts';
import { Product } from '../../model/product.ts';
import { ToJson } from '../../shared/utils/ToJson.tsx';
import { ProductCard } from './components/ProductCard.tsx';

export function Shop() {

  const addToCart = useCart(state => state.addToCart)
  const products = useLoaderData() as Product[];
  const openSummaryPanel = useCartSummaryPanel(state => state.open)

  return <div>

    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4 ">
    {
      products.map(p => {
        return <ProductCard
          product={p}
          key={p.id}
          onAddToCart={() => {

            addToCart(p);
            openSummaryPanel();
          }}
        />
      })
    }
    </div>



   <ToJson value={products} />
  </div>
}


