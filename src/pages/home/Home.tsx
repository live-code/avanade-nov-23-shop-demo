import { useEffect, useState } from 'react';
import { Button, Fx, MapQuest, Panel, Title } from '../../shared/';
import { ButtonGroup } from '../../shared/components/ButtonGroup.tsx';
import { Leaflet } from '../../shared/components/Leaflet.tsx';

export function Home() {
  const [myCoords, setMyCoords] = useState<[number, number]>([43, 13])
  const [zoom, setZoom] = useState(5)
  return <div>

    <Button onClick={() => setMyCoords([43, 13])}>Centro italia</Button>
    <Button onClick={() => setMyCoords([45, 15])}>Sud italia</Button>
    <Button onClick={() => setMyCoords([49, 11])}>ANother</Button>
    <Leaflet coords={myCoords} zoom={zoom}/>

    <Button onClick={() => setZoom(z => z - 1)}>-</Button>
    <Button onClick={() => setZoom(z => z + 1)}>+</Button>

    <Title>Home</Title>

    <Panel title="Ciccio">
      <button className="btn">do something</button>
    </Panel>


    <br/>

    <ButtonGroup direction="column" >
      <Button
        onMouseOut={() => console.log('mouse out 2')}
        icon="fa fa-google"
        onClick={() => console.log('click 2')}
        disabled={false}
        className="border-2 border-black border-dashed"
        style={{color: 'red'}}
      > LABEL </Button>

      <Button> LABEL 2</Button>
      <Button> LABEL 3</Button>
      <Button> LABEL 4</Button>
    </ButtonGroup>




    <MapQuest
      alt="Mappa di milano"
      style={{border: '3px dashed blue'}}
      width="50%"
      city="Milano" zoom={10}
    />
    <MapQuest
      city="Milano" zoom={5} />


  </div>
}
