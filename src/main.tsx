import ReactDOM from 'react-dom/client'
import { createBrowserRouter, Navigate, RouterProvider } from 'react-router-dom';
import App from './App.tsx'
import './index.css'
import 'font-awesome/css/font-awesome.min.css'
import { Cart } from './pages/cart/Cart.tsx';
import { Home } from './pages/home/Home.tsx';
import { Shop } from './pages/shop/Shop.tsx';
import { UiKitPage } from './pages/uikit/UiKitPage.tsx';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: 'home',
        element: <Home />
      },
      {
        path: 'cart',
        element: <Cart />
      },
      {
        path: 'shop',
        element: <Shop />,
        loader: () => {
          return fetch('http://localhost:3000/products')
        }
      },
      {
        path: '',
        element: <Navigate to="home" />
      },
      {
        path: 'uikit',
        element: <UiKitPage />
      },
    ]
  }
])
ReactDOM.createRoot(document.getElementById('root')!).render(
   <RouterProvider router={router} />
)
